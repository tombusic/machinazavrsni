﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateAroundTarget : MonoBehaviour
{

    public Vector3 Axis;

    public Transform Target;

    public int Speed;

    // Use this for initialization
    void Start()
    {

    }

    //Update is called once per frames

    void Update()
    {
        if (Target != null)
        {
            transform.RotateAround(Target.position, Axis, Speed * Time.deltaTime);
        }
    }
   
}
