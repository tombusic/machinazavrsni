﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Vuforia;


//služi za upravljanje button eventima
public class UIManager : MonoBehaviour {


   

    //Funkcija Play/Stop buttona
    public void pauseApplication()
    {
        if (Time.timeScale == 1)
        {
            Time.timeScale = 0;
        }
        else
        {
            Time.timeScale = 1;
        }
    }

    public void LoadCreditsScene() {

        SceneManager.LoadScene("CreditsScene", LoadSceneMode.Single);

    }

    public void backToAplicationScene()
    {

        SceneManager.LoadScene("ApplicationScene", LoadSceneMode.Single);

    }

    //funkcija ButtonaExit
    public void ExitAplication()
    {
        Application.Quit();
    }

}
    
