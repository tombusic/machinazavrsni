﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlanetSelect : MonoBehaviour
{

    private GameObject infPanel;
    [SerializeField]
    private string planetDiscriptionText = null;

    private void Awake()
    {
        infPanel = GameObject.FindGameObjectWithTag("InfPanel");
    }


    private void OnMouseDown()
    {
        //Debug.Log(""+planetDiscriptionText);
        infPanel.GetComponentInChildren<Text>().text = planetDiscriptionText;
    }
}

