﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


//upravlja touch eventima u aplikaciji
//registrira touch na pojedini planet te ispisuje informacije o istome
public class PlanetSelect2 : MonoBehaviour {

    [SerializeField]
    private string[] planetDiscriptionText = new string[9];
    private GameObject infPanel;

   
    private void Awake()
    {
        infPanel = GameObject.FindGameObjectWithTag("InfPanel");

    }


    // Use this for initialization
    void Start () {
		
	}

    // Update is called once per frame
    void Update() {

        if (Input.GetMouseButtonDown(0)) {


            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out hit))
            {



                switch (hit.transform.name)
                {
                    case "Sun":
                        infPanel.GetComponentInChildren<Text>().text = planetDiscriptionText[0];
                        break;
                    case "Mercury":
                        infPanel.GetComponentInChildren<Text>().text = planetDiscriptionText[1];
                        break;
                    case "Venus":
                        infPanel.GetComponentInChildren<Text>().text = planetDiscriptionText[2];
                        break;
                    case "Earth":
                        infPanel.GetComponentInChildren<Text>().text = planetDiscriptionText[3];
                        break;
                    case "Mars":
                        infPanel.GetComponentInChildren<Text>().text = planetDiscriptionText[4];
                        break;
                    case "Jupiter":
                        infPanel.GetComponentInChildren<Text>().text = planetDiscriptionText[5];
                        break;
                    case "Saturn":
                        infPanel.GetComponentInChildren<Text>().text = planetDiscriptionText[6];
                        break;
                    case "Uranus":
                        infPanel.GetComponentInChildren<Text>().text = planetDiscriptionText[7];
                        break;
                    case "Neptun":
                        infPanel.GetComponentInChildren<Text>().text = planetDiscriptionText[8];
                        break;
                    default:
                        break;


                }

            }

            //ukoliko klik ide u prazno defaultna poruka
            else
            {
                infPanel.GetComponentInChildren<Text>().text = "Dodirnite pojedini planet za informacije...";
            }
            }

        }

    }


















            /*{


        //aktivacija child objekta InformationPanela
        foreach (Transform child in infPanel.transform)
        {
            child.gameObject.SetActive(true);
        }

        RaycastHit hit;
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        if (Physics.Raycast(ray, out hit))
        {


            if (hit.transform.name == "Sun")
            {
                Debug.Log(hit.transform.name);
                //infPanel.transform.GetChild(0).gameObject.SetActive(true);
                infPanel.GetComponentInChildren<Text>(true).text = "sun";
            }

            else if (hit.transform.name == "Earth")
            {
                Debug.Log(hit.transform.name);
                //infPanel.transform.GetChild(0).gameObject.SetActive(true);
                infPanel.GetComponentInChildren<Text>().text = "Earth";
            }

            else
            {

            }



        }
        //ukoliko nije kliknut niti jedan planet napiši defaultnu poruku
        else
        {
            infPanel.GetComponentInChildren<Text>().gameObject.SetActive(true);
            infPanel.GetComponentInChildren<Text>().text = "TOUCH PLANET FOR INFORMATION";
        }

    }*/

        

   

